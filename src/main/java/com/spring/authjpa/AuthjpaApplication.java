package com.spring.authjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AuthjpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthjpaApplication.class, args);
    }

}
