package com.spring.authjpa.auth.entity;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "authorities")
public class Authority implements Serializable {
  @Id @GeneratedValue private int id;

  @Column(length = 20)
  private String name;
}
