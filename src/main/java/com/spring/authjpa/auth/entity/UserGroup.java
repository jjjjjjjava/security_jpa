package com.spring.authjpa.auth.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "group_members")
public class UserGroup {
  @Id @GeneratedValue private int id;

  @Column(name = "username", length = 64)
  private String username;

  @Column(name = "group_id")
  private int groupId;
}
