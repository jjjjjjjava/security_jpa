package com.spring.authjpa.auth.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Data
@Entity
@Table(name = "groups")
public class Group implements Serializable {
  @Id @GeneratedValue private int id;

  @Column(length = 30)
  private String name;

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(
      name = "group_authorities",
      joinColumns = {@JoinColumn(name = "group_id", referencedColumnName = "id")},
      inverseJoinColumns = {@JoinColumn(name = "authority_id", referencedColumnName = "id")})
  private List<Authority> authorityList;
}
