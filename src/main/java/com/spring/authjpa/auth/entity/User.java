package com.spring.authjpa.auth.entity;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "users")
@Data
public class User implements Serializable {
  @Id
  @Column(name = "uuid", length = 64)
  @GenericGenerator(name = "idGenerator", strategy = "uuid2")
  @GeneratedValue(generator = "idGenerator")
  private String uuid;

  @Column(length = 64, nullable = false)
  private String password;

  @Column(length = 40, nullable = false)
  private String username;

  @Column(length = 11)
  private String telephone;

  @Column(length = 40)
  private String email;

  private int enabled;

  @Column(length = 1)
  private int sex;

  @Column(length = 3)
  private int age;

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(
      name = "group_members",
      joinColumns = {@JoinColumn(name = "username", referencedColumnName = "username")},
      inverseJoinColumns = {@JoinColumn(name = "group_id", referencedColumnName = "id")})
  private List<Group> groupList;
}
