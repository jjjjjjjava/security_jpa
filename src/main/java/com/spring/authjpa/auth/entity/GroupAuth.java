package com.spring.authjpa.auth.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "group_authorities")
public class GroupAuth {
  @Id @GeneratedValue private int id;

  @Column(name = "group_id")
  private int groupId;

  @Column(name = "authority_id")
  private int authorityId;
}
