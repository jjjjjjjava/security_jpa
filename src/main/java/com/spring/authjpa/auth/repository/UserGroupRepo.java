package com.spring.authjpa.auth.repository;

import com.spring.authjpa.auth.entity.UserGroup;
import org.springframework.data.repository.CrudRepository;

public interface UserGroupRepo extends CrudRepository<UserGroup, Integer> {}
