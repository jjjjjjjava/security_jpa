package com.spring.authjpa.auth.repository;

import com.spring.authjpa.auth.entity.GroupAuth;
import org.springframework.data.repository.CrudRepository;

public interface GroupAuthRepo extends CrudRepository<GroupAuth, Integer> {}
