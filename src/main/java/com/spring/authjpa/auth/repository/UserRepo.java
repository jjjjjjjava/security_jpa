package com.spring.authjpa.auth.repository;

import com.spring.authjpa.auth.entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserRepo extends PagingAndSortingRepository<User, String> {}
