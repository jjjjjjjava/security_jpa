package com.spring.authjpa.auth.repository;

import com.spring.authjpa.auth.entity.Authority;
import org.springframework.data.repository.CrudRepository;

public interface AuthRepo extends CrudRepository<Authority, Integer> {}
