package com.spring.authjpa.auth.repository;

import com.spring.authjpa.auth.entity.Group;
import org.springframework.data.repository.CrudRepository;

public interface GroupRepo extends CrudRepository<Group, Integer> {}
