package com.spring.authjpa.common.system.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.sql.DataSource;

@Slf4j
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
  private final DataSource dataSource;

  @Autowired
  public SecurityConfig(DataSource dataSource) {
    this.dataSource = dataSource;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests()
        .antMatchers("/css/**", "index")
        .permitAll()
        .antMatchers("/user/**")
        .hasRole("USER")
        .and()
        .formLogin()
        .loginPage("/login")
        .failureUrl("/login-error");
  }

  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    auth.jdbcAuthentication()
        .dataSource(dataSource)
        .usersByUsernameQuery("select username,password,enabled from users where username = ?")
        .authoritiesByUsernameQuery(
            "select gm.username,a.name from group_members gm ,groups g,group_authorities ga,authorities a where gm.username=? and g.id = ga.group_id and g.id = gm.group_id and ga.authority_id = a.id")
        .groupAuthoritiesByUsername(
            "select g.id,g.name,a.name from group_members gm ,groups g,group_authorities ga,authorities a where gm.username=? and g.id = ga.group_id and g.id = gm.group_id and ga.authority_id = a.id")
        .passwordEncoder(new BCryptPasswordEncoder());
  }
}
