package com.spring.authjpa.common.system.config;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author JackYoung
 * @date 2019/4/7 23:36
 */
@EnableJpaRepositories(basePackages = {"com.spring.authjpa.auth.repository"})
public class RepoConfig {}
