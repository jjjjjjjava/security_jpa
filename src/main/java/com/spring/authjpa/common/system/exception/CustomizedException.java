package com.spring.authjpa.common.system.exception;

/**
 * @author JackYoung
 * @date 2019/4/6 12:59
 */
public class CustomizedException extends RuntimeException {
  public CustomizedException() {
    super();
  }

  public CustomizedException(String message) {
    super(message);
  }

  public CustomizedException(String message, Throwable cause) {
    super(message, cause);
  }

  public CustomizedException(Throwable cause) {
    super(cause);
  }

  protected CustomizedException(
      String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
