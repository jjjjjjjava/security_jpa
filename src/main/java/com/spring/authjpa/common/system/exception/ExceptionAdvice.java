package com.spring.authjpa.common.system.exception;

import com.spring.authjpa.common.pojo.Result;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public Result catchException(HttpServletRequest req, Exception e){
        return Result.error(e.toString());
    }
}
