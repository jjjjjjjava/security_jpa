package com.spring.authjpa.common.pojo;

public enum ResultCode {
    SUCCESS("成功",0),
    ERROR("系统繁忙",40000);
    private String msg;
    private Integer code;
    ResultCode(String msg,Integer code){
        this.msg = msg;
        this.code = code;
    }
    public String getMsg(){
        return msg;
    }
    public Integer getCode(){
        return code;
    }
}
