package com.spring.authjpa.common.pojo;

import lombok.Data;

@Data
public class Result {
  private String msg;
  private Integer code;
  private Object data;
  private ResultCode resultCode;

  public static Result success() {
    return packageResult(ResultCode.SUCCESS, null);
  }

  public static Result success(Object data) {
    return packageResult(ResultCode.SUCCESS, data);
  }

  public static Result error() {
    return packageResult(ResultCode.ERROR, null);
  }

  public static Result error(Object data) {
    return packageResult(ResultCode.ERROR, data);
  }

  private static Result packageResult(ResultCode resultCode, Object data) {
    Result result = new Result();
    result.setMsg(resultCode.getMsg());
    result.setCode(resultCode.getCode());
    result.setData(data);
    return result;
  }
}
