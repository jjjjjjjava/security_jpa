package com.spring.authjpa;

import com.spring.authjpa.auth.entity.*;
import com.spring.authjpa.auth.repository.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AuthjpaApplicationTests {
  @Autowired UserRepo userRepo;
  @Autowired AuthRepo authRepo;
  @Autowired UserGroupRepo userGroupRepo;
  @Autowired GroupAuthRepo groupAuthRepo;
  @Autowired GroupRepo groupRepo;

  @Test
  public void insertUser() {
    User user = new User();
    user.setUsername("张三");
    user.setPassword(new BCryptPasswordEncoder().encode("123456"));
    user.setTelephone("13277848573");
    user.setEmail("1285076236@qq.com");
    user.setEnabled(1);
    user.setSex(1);
    user.setAge(29);
    user = userRepo.save(user);
    Authority authority = new Authority();
    authority.setName("ROLE_USER");
    authority = authRepo.save(authority);
    Group group = new Group();
    group.setName("测试一组");
    group = groupRepo.save(group);
    UserGroup userGroup = new UserGroup();
    userGroup.setUsername(user.getUsername());
    userGroup.setGroupId(group.getId());
    userGroupRepo.save(userGroup);
    GroupAuth groupAuth = new GroupAuth();
    groupAuth.setGroupId(group.getId());
    groupAuth.setAuthorityId(authority.getId());
    groupAuthRepo.save(groupAuth);
  }

  @Test
  public void queryUsers() {
    Pageable pageable = PageRequest.of(0, 10);
    Page<User> page = userRepo.findAll(pageable);
    //        Iterable<UsersEntity> it = usersRepo.findAll();
    //        it.forEach(usersEntity -> {
    //            System.out.println(usersEntity);
    //        });
    System.out.println(page.getContent());
  }

  @Test
  public void updateUser() {
    //       生成环境需密码加密
    //        MD5Util.md5("abc1234567890");
    User user = new User();
    user.setUsername("张三");
    user.setUuid("05a9d529-e5c6-451f-b802-ee635e8d67b3");
    user.setPassword("123456");
    System.out.println(user);
    userRepo.save(user);
  }
}
